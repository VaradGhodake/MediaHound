# MediaHound

Django MVC and Last.FM API based Music and Video storage webApp

## Getting Started

Clone this repository and install packages from requirements.txt using Pip  

Run Django server and open localhost:8000/ to use this app  


### Prerequisites


```
Python 2.7  
beautifulsoup4==4.6.0  
billboard.py==4.2.0  
certifi==2018.1.18  
chardet==3.0.4  
Django==1.11.11  
idna==2.6  
pylast==2.2.0  
pytz==2018.3  
requests==2.18.4  
six==1.11.0  
urllib3==1.22  

```

### Installing

Clone the repo using  
```
git clone https://gitlab.com/VaradGhodake/MediaHound.git
```

Install requirements

```
pip install -r requirements.txt  
```

Start Django Server after changing directory

```
python manage.py runserver   
```

Navigate to your browser and visit

```
http://127.0.0.1:8000/  
```

## Application Flow
![Application Flow](flow.png)  



## Built With

* [Django](https://www.djangoproject.com) - Web Framework for python
* [VirtualEnv](https://virtualenv.pypa.io/en/stable/) - Virtual Environments in Python
* [LastFM API](https://www.last.fm/api) - LastFM  Music API 
* [Bootstrap](https://getbootstrap.com/) - Twitter Bootstrap for responsiveness
* [JavaScript](https://www.javascript.com/) - For event handling
* [Glyphicons](https://www.w3schools.com/bootstrap/bootstrap_glyphicons.asp) - Icons


## Authors

* [**Varad Ghodake**](https://gitlab.com/VaradGhodake)


## Acknowledgments

* Hat tip to anyone whose code was used
