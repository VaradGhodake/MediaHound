from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from .forms import UserForm, AlbumForm, SongForm, VideoForm
from .models import Album, Song, Video
from urllib import urlopen as make_request
import json
from django.db.models import Q

IMAGE_FILE_TYPES = ['png', 'jpg', 'jpeg']
AUDIO_FILE_TYPES = ['wav', 'mp3', 'ogg']
VIDEO_FILE_TYPES = ['mp4', 'mkv']

def index (request):
    if not request.user.is_authenticated():
        return render(request, 'engine/login.html')
    else:
        albums = Album.objects.filter(user=request.user)
        song_results = Song.objects.all()
        query = request.GET.get("q")
        if query:
            albums = albums.filter(
                Q(album_title__icontains=query) |
                Q(artist__icontains=query)
            ).distinct()
            song_results = song_results.filter(
                Q(song_title__icontains=query)
            ).distinct()
            return render(request, 'engine/index.html', {
                'albums': albums,
                'songs': song_results,
            })
        else:
            albums = Album.objects.filter(user=request.user)
            return render(request, 'engine/index.html', {'albums': albums})


def charts (request):
    req = make_request("http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=india&limit=6&api_key=adf7412dad14f4430871365b20158495&format=json")
    india_charts = json.loads(req.read())
    artist_list = []
    for artist in india_charts['topartists']['artist']:
         artist_list.append({
         "name": artist['name'],
         "image": artist['image'][3]['#text'],
         "listeners": artist['listeners'],
         "url": artist['url']  
         })
         
    req = make_request("http://ws.audioscrobbler.com/2.0/?method=geo.gettoptracks&country=india&api_key=adf7412dad14f4430871365b20158495&format=json")
    india_charts = json.loads(req.read())
    artist_list = []
    for artist in india_charts['topartists']['artist']:
         artist_list.append({
         "name": artist['name'],
         "image": artist['image'][3]['#text'],
         "listeners": artist['listeners'],
         "url": artist['url']  
         })
    # print artist_list
    return render(request, 'engine/charts.html', {'india_charts': artist_list})


def logout_user(request):
    logout(request)
    form = UserForm(request.POST or None)
    context = {
        "form": form,
    }
    return render(request, 'engine/login.html', context)


def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                albums = Album.objects.filter(user=request.user)
                return render(request, 'engine/index.html')
            else:
                return render(request, 'engine/login.html', {'error_message': 'Your account has been disabled'})
        else:
            return render(request, 'engine/login.html', {'error_message': 'Invalid login'})
    return render(request, 'engine/login.html')


def register(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                albums = Album.objects.filter(user=request.user)
                return render(request, 'engine/index.html', {'albums': albums})
    context = {
        "form": form,
    }
    return render(request, 'engine/register.html', context)

def create_album(request):
    if not request.user.is_authenticated():
        return render(request, 'engine/login.html')
    else:
        form = AlbumForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            album = form.save(commit=False)
            album.user = request.user
            album.album_logo = request.FILES['album_logo']
            file_type = album.album_logo.url.split('.')[-1]
            file_type = file_type.lower()
            if file_type not in IMAGE_FILE_TYPES:
                context = {
                    'album': album,
                    'form': form,
                    'error_message': 'Image file must be PNG, JPG, or JPEG',
                }
                return render(request, 'engine/create_album.html', context)
            album.save()
            return render(request, 'engine/detail.html', {'album': album})
        context = {
            "form": form,
        }
        return render(request, 'engine/create_album.html', context)
        
def detail(request, album_id):
    if not request.user.is_authenticated():
        return render(request, 'engine/login.html')
    else:
        user = request.user
        album = get_object_or_404(Album, pk=album_id)
        return render(request, 'engine/detail.html', {'album': album, 'user': user})

def favorite_album(request, album_id):
    album = get_object_or_404(Album, pk=album_id)
    try:
        if album.is_favorite:
            album.is_favorite = False
        else:
            album.is_favorite = True
        album.save()
    except (KeyError, Album.DoesNotExist):
        return JsonResponse({'success': False})
    else:
        return JsonResponse({'success': True})

def delete_album(request, album_id):
    album = Album.objects.get(pk=album_id)
    album.delete()
    albums = Album.objects.filter(user=request.user)
    return render(request, 'engine/index.html', {'albums': albums})
    
def create_song(request, album_id):
    form = SongForm(request.POST or None, request.FILES or None)
    album = get_object_or_404(Album, pk=album_id)
    if form.is_valid():
        albums_songs = album.song_set.all()
        for s in albums_songs:
            if s.song_title == form.cleaned_data.get("song_title"):
                context = {
                    'album': album,
                    'form': form,
                    'error_message': 'You already added that song',
                }
                return render(request, 'engine/create_song.html', context)
        song = form.save(commit=False)
        song.album = album
        song.audio_file = request.FILES['audio_file']
        file_type = song.audio_file.url.split('.')[-1]
        file_type = file_type.lower()
        if file_type not in AUDIO_FILE_TYPES:
            context = {
                'album': album,
                'form': form,
                'error_message': 'Audio file must be WAV, MP3, or OGG',
            }
            return render(request, 'engine/create_song.html', context)

        song.save()
        return render(request, 'engine/detail.html', {'album': album})
    context = {
        'album': album,
        'form': form,
    }
    return render(request, 'engine/create_song.html', context)

def delete_song(request, album_id, song_id):
    album = get_object_or_404(Album, pk=album_id)
    song = Song.objects.get(pk=song_id)
    song.delete()
    return render(request, 'engine/detail.html', {'album': album})
    
def favoriteSong(request, song_id):
    song = get_object_or_404(Song, pk=song_id)
    try:
        if song.is_favorite:
            song.is_favorite = False
        else:
            song.is_favorite = True
        song.save()
    except (KeyError, Song.DoesNotExist):
        return JsonResponse({'success': False})
    else:
        return JsonResponse({'success': True})
        
def songs(request, filter_by):
    if not request.user.is_authenticated():
        return render(request, 'engine/login.html')
    else:
        try:
            song_ids = []
            for album in Album.objects.filter(user=request.user):
                for song in album.song_set.all():
                    song_ids.append(song.pk)
            users_songs = Song.objects.filter(pk__in=song_ids)
            if filter_by == 'favorites':
                users_songs = users_songs.filter(is_favorite=True)
        except Album.DoesNotExist:
            users_songs = []
        return render(request, 'engine/songs.html', {
            'song_list': users_songs,
            'filter_by': filter_by,
        })

def create_video(request, album_id):
    form = VideoForm(request.POST or None, request.FILES or None)
    album = get_object_or_404(Album, pk=album_id)
    if form.is_valid():
        albums_videos = album.video_set.all()
        for v in albums_videos:
            if v.video_title == form.cleaned_data.get("video_title"):
                context = {
                    'album': album,
                    'form': form,
                    'error_message': 'You already added that video',
                }
                return render(request, 'engine/create_video.html', context)
        video = form.save(commit=False)
        video.album = album
        video.video_file = request.FILES['video_file']
        file_type = video.video_file.url.split('.')[-1]
        file_type = file_type.lower()
        if file_type not in VIDEO_FILE_TYPES:
            context = {
                'album': album,
                'form': form,
                'error_message': 'Video file must be MP4',
            }
            return render(request, 'engine/create_video.html', context)

        video.save()
        return render(request, 'engine/detail.html', {'album': album})
    context = {
        'album': album,
        'form': form,
    }
    return render(request, 'engine/create_video.html', context)

def delete_video(request, album_id, song_id):
    album = get_object_or_404(Album, pk=album_id)
    video = Video.objects.get(pk=video_id)
    video.delete()
    return render(request, 'engine/detail.html', {'album': album})

def favoriteVideo(request, video_id):
    video = get_object_or_404(Video, pk=video_id)
    try:
        if video.is_favorite:
            video.is_favorite = False
        else:
            video.is_favorite = True
        video.save()
    except (KeyError, Video.DoesNotExist):
        return JsonResponse({'success': False})
    else:
        return JsonResponse({'success': True})

def videos(request, filter_by):
    if not request.user.is_authenticated():
        return render(request, 'engine/login.html')
    else:
        try:
            video_ids = []
            for album in Album.objects.filter(user=request.user):
                for video in album.video_set.all():
                    video_ids.append(video.pk)
            users_videos = Video.objects.filter(pk__in=video_ids)
            if filter_by == 'favorites':
                users_videos = users_videos.filter(is_favorite=True)
        except Album.DoesNotExist:
            users_videos = []
        return render(request, 'engine/videos.html', {
            'video_list': users_videos,
            'filter_by': filter_by,
        })
    render(request, "Works")


def charts (request):
    req = make_request("http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=india&limit=6&api_key=adf7412dad14f4430871365b20158495&format=json")
    india_charts = json.loads(req.read())
    artist_list = []
    for artist in india_charts['topartists']['artist']:
         artist_list.append({
             "name": artist['name'],
             "image": artist['image'][3]['#text'],
             "listeners": artist['listeners'],
             "url": artist['url']  
         })
    #print artist_list     
    
    req = make_request("http://ws.audioscrobbler.com/2.0/?method=geo.gettoptracks&country=india&limit=30&api_key=adf7412dad14f4430871365b20158495&format=json")
    india_tracks = json.loads(req.read())
    track_list = []
    for track in india_tracks['tracks']['track']:
         track_list.append({
             "name": track['name'],
             "listeners": track['listeners'],
             "url": track['url']  
         })
    #print track_list 
    return render(request, 'engine/charts.html', {'india_charts': artist_list, 'india_tracks': track_list})
